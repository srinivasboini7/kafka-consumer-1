package com.srini.kafkaconsumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

/**
 * The type Consumer cfg.
 */
@Configuration
@Slf4j
public class ConsumerCfg {
}
